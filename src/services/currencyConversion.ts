import axios from 'axios'
import { useAppStore } from '@/store/app'

import type { Currency, ConversionRate } from '@/src/types/currency'

const apiKey = 'demo'; // there is a daily limit of 25 requests on non-demo end-points

function useCurrencyConversion() {
    const store = useAppStore()

    async function fetchCurrencyConversion(from: Currency, to: Currency): Promise<ConversionRate> {
        const apiFunction = 'CURRENCY_EXCHANGE_RATE'

        store.setLoading(true);
        return axios
            .get(
                `https://www.alphavantage.co/query?function=${apiFunction}&from_currency=${from}&to_currency=${to}&apikey=${apiKey}`
            )
            .then((r) => {
                if (r.data.Information) {
                    if (r.data.Information === "Thank you for using Alpha Vantage! Our standard API rate limit is 25 requests per day. Please subscribe to any of the premium plans at https://www.alphavantage.co/premium/ to instantly remove all daily rate limits.") {
                        throw new Error("API limit is reached")
                    }
                    throw new Error("An error occurred")
                }

                const payload = r.data["Realtime Currency Exchange Rate"]

                return {
                    from: payload["1. From_Currency Code"] as Currency,
                    fromFullName: payload["2. From_Currency Name"] as string,
                    to: payload["3. To_Currency Code"] as Currency,
                    toFullName: payload["4. To_Currency Name"] as string,
                    rate: payload["5. Exchange Rate"] as string, // of type number
                    timestamp: payload["6. Last Refreshed"] as string, // of type date (UTC)
                    bidRate: payload["8. Bid Price"] as string, // of type number
                    askRate: payload["9. Ask Price"] as string, // of type number
                }
            })
            .catch(e => {
                throw e;
            })
            .finally(() => {
                store.setLoading(false);
            })
    }

    return {
        fetchCurrencyConversion
    }
}

export default useCurrencyConversion