// Utilities
import { defineStore } from 'pinia'

export const useAppStore = defineStore('app', {
  state: () => ({
    loading: false
  }),

  actions: {
    setLoading(newState: boolean) {
      this.loading = newState
    }
  }
})
