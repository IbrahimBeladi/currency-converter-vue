export type Currency = "USD" | "JPY" | "BTC" // ...

export type Trend = 'increase' | 'decrease' | 'equal' | null

export type ConversionRate = {
    from: Currency,
    fromFullName: string,
    to: Currency,
    toFullName: string,
    rate: string, // of type number
    timestamp: string, // of type date (UTC)
    bidRate: string, // of type number
    askRate: string, // of type number
    trend?: Trend
}
